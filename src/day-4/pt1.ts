import fs from "fs/promises";

interface Task {
    readonly lower: number;
    readonly upper: number;
}

function parseTask(str: string): Task {
    const values = str.split("-");
    return {
        lower: Number.parseInt(values[0]),
        upper: Number.parseInt(values[1]),
    }
}

function contains(t1: Task, t2: Task) {
    return t1.lower <= t2.lower && t2.upper <= t1.upper;
}

const input = await fs.readFile("src/day-4/input.txt", "utf8");

const taskPairs = input.split(/\r?\n/).map(line => line.split(",")).map(array => {
    return { left: parseTask(array[0]), right: parseTask(array[1]) };
});

const contained = taskPairs.reduce((acc, curr) => {
    const isContained = contains(curr.left, curr.right) || contains(curr.right, curr.left);

    if (isContained)
        return acc + 1;
    
    return acc;
}, 0);


// Expected: 536
console.log(contained);

// Run using:
// npx node --loader ts-node/esm src/day-3/pt1.ts
