import fs from "fs/promises";

const aCode = "a".charCodeAt(0);
const ACode = "A".charCodeAt(0);
function getPriority(char: string) {
    const isUpperCase = char === char.toUpperCase();
    const difference = isUpperCase ? ACode - 27 : aCode - 1;
    const priority = char.charCodeAt(0) - difference;
    return priority;
}

function groupByThree<T>([a, b, c, ...rest]: T[]): T[][] {
    if (rest.length === 0) return [[a, b, c]];

    return [[a, b, c], ...groupByThree(rest)];
}

const input = await fs.readFile("src/day-3/input.txt", "utf8");

const backpacks = groupByThree(input.split(/\r?\n/).map(line => line.split(""))).map(group => ({
    first: group[0],
    second: group[1],
    third: group[2],
}));

const intersections = backpacks.map(backpack => {
    const thirdSet = new Set(backpack.third);
    const secondSet = new Set(backpack.second);

    return backpack.first.filter(v => thirdSet.has(v) && secondSet.has(v))[0];
});

const value = intersections.reduce((prev, curr) => {
    const priority = getPriority(curr);

    return prev + priority;
}, 0);

// Expected: 2881
console.log(value);

// Run using:
// npx node --loader ts-node/esm src/day-3/pt1.ts
