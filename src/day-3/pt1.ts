import fs from "fs/promises";

const aCode = "a".charCodeAt(0);
const ACode = "A".charCodeAt(0);
function getPriority(char: string) {
    const isUpperCase = char === char.toUpperCase();
    const difference = isUpperCase ? ACode - 27 : aCode - 1;
    const priority = char.charCodeAt(0) - difference;
    return priority;
}

const input = await fs.readFile("src/day-3/input.txt", "utf8");

const backpacks = input.split(/\r?\n/).map(line => line.split("")).map(array => {
    const left = array.splice(0, array.length / 2);

    return { left, right: array };
});

const intersections = backpacks.map(backpack => {
    const rightSet = new Set(backpack.right);

    const intersection = backpack.left.filter(v => rightSet.has(v))[0];

    return intersection;
})

const value = intersections.reduce((prev, curr) => {
    const priority = getPriority(curr);

    return prev + priority;
}, 0);

// Expected: 7980
console.log(value);

// Run using:
// npx node --loader ts-node/esm src/day-3/pt1.ts
