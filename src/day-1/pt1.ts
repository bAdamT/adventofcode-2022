import fs from "fs/promises";

const input = await fs.readFile("src/day-1/input.txt", "utf8");

const backpacks = input.split(/\r?\n\r?\n/).map(paragraph => paragraph.split(/\r?\n/).map(line => Number.parseInt(line)));

const calories = backpacks.map(bp => bp.reduce((v, sum) => v + sum, 0));

const max = Math.max(...calories);

// Expected: 67658
console.log(max);

// Run using:
// npx node --loader ts-node/esm src/day-1/pt1.ts