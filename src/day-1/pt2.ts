import fs from "fs/promises";

const input = await fs.readFile("src/day-1/input.txt", "utf8");

const backpacks = input.split(/\r?\n\r?\n/).map(paragraph => paragraph.split(/\r?\n/).map(line => Number.parseInt(line)));

const calories = backpacks.map(bp => bp.reduce(sumReducer, 0));

const firstThree = calories.splice(0, 3);
let candidates = firstThree;
let weakestCandididate = Math.min(...candidates);
let weakestIndex = candidates.reduce(minIndexReducer, 0);
let total = firstThree.reduce((v, sum) => v + sum, 0);

for (const current of calories) {
    if (current > weakestCandididate) {
        // Update total
        total = total - weakestCandididate + current;

        // Update candidates
        candidates[weakestIndex] = current;
        weakestCandididate = Math.min(...candidates);
        weakestIndex = candidates.reduce(minIndexReducer, 0);
    }
}

console.log(total);

function minIndexReducer(previousIndex: number, current: number, currentIndex: number, array: number[]) {
    const previousValue = array[previousIndex];
    if (previousValue > current) {
        return currentIndex;
    }
    
    return previousIndex
}
    
function sumReducer(sum: number, v: number) {
    return sum + v
};