import fs from "fs/promises";

function crateNumberToIndex(crateNumber: number): number {
    if (crateNumber === 1)
        return 1;
    
    if (crateNumber > 1 && crateNumber < 10)
        return crateNumberToIndex(crateNumber - 1) + 4;
    
    return 0;
}

interface Instruction {
    readonly from: number;
    readonly to: number;
    readonly count: number;
}
 
function executeInstruction(crates: string[][], instruction: Instruction) {
    const from = crates[instruction.from - 1];
    const to = crates[instruction.to - 1];

    const toMove = from.splice(0, instruction.count);
    toMove.reverse();
    to.unshift(...toMove)
}

const input = await fs.readFile("src/day-5/input.txt", "utf8");
const inputLines = input.split(/\r?\n/);

const crateNumberLineIndex = inputLines.findIndex(line => line.startsWith(" 1 "))!;
const crateNumberLine = inputLines[crateNumberLineIndex];
const crateCount = Number.parseInt(crateNumberLine.charAt(crateNumberLine.length - 2)); // Ends in number and then a space

let crates = Array.from(Array(crateCount), () => [] as string[]);
for (let lineIndex = 0; lineIndex < crateNumberLineIndex; lineIndex++) 
    for (let crateIndex = 1; crateIndex <= crateCount; crateIndex++) {
        crates[crateIndex - 1].push(inputLines[lineIndex][crateNumberToIndex(crateIndex)])
    }
crates = crates.map(crate => crate.filter(box => box !== " "));

inputLines.splice(0, crateNumberLineIndex + 2);
const regex = /move (\d*) from (\d*) to (\d*)/;
const instructions = inputLines.map(line => {
    const match = line.match(regex);
    return {
        count: Number.parseInt(match![1]),
        from: Number.parseInt(match![2]),
        to: Number.parseInt(match![3]),
    } as Instruction;
});

for (const instruction of instructions) {
    executeInstruction(crates, instruction);
}

// Expected: ZBDRNPMVH
console.log(crates.map(c => c[0])); 

// Run using:
// npx node --loader ts-node/esm src/day-3/pt1.ts
