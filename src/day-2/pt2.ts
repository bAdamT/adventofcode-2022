import fs from "fs/promises";

type names = "rock" | "paper" | "scissors";
type outcomes = "loss" | "draw" | "win";

const signToName: {[key: string]: names} = {
    "A": "rock",
    "B": "paper",
    "C": "scissors",
};

const signToOutcome: {[key: string]: outcomes} = {
    "X": "loss",
    "Y": "draw",
    "Z": "win",
};


const namesToValue: {[key in names]: number}  = {
    "rock": 1,
    "paper": 2,
    "scissors": 3,
}

const pairsAndOutcome: [names, names, outcomes][] = [
    ["rock", "rock", "draw"],
    ["rock", "paper", "win"],
    ["rock", "scissors", "loss"],
    ["paper", "paper", "draw"],
    ["paper", "rock", "loss"],
    ["paper", "scissors", "win"],
    ["scissors", "scissors", "draw"],
    ["scissors", "rock", "win"],
    ["scissors", "paper", "loss"],
];

const outcomeTovalue = {
    "win": 6,
    "draw": 3,
    "loss": 0,
}

const getMove = (left: names, right: outcomes) => {
    return pairsAndOutcome.find(line => line[0] === left && line[2] === right)![1];
}

const input = await fs.readFile("src/day-2/input.txt", "utf8");

const rounds = input.split(/\r?\n/).map(round => round.split(" ")).map(round => [signToName[round[0]], signToOutcome[round[1]]] as [names, outcomes]);

const result = rounds.reduce((acc, curr) => {
    const opp = curr[0];
    const outcome = curr[1];

    const you = getMove(opp, outcome)
    return acc + namesToValue[you] + outcomeTovalue[outcome];
}, 0);

// Expected: 12586
console.log(result);

// Run using:
// npx node --loader ts-node/esm src/day-1/pt1.ts