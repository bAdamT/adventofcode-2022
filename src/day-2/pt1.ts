import fs from "fs/promises";

type names = "rock" | "paper" | "scissors";
type outcomes = "loss" | "draw" | "win";

const signToName: {[key: string]: names} = {
    "A": "rock",
    "X": "rock",
    "B": "paper",
    "Y": "paper",
    "C": "scissors",
    "Z": "scissors",
};

const namesToValue: {[key in names]: number}  = {
    "rock": 1,
    "paper": 2,
    "scissors": 3,
}

const pairsAndOutcome: [names, names, outcomes][] = [
    ["rock", "rock", "draw"],
    ["rock", "paper", "win"],
    ["rock", "scissors", "loss"],
    ["paper", "paper", "draw"],
    ["paper", "rock", "loss"],
    ["paper", "scissors", "win"],
    ["scissors", "scissors", "draw"],
    ["scissors", "rock", "win"],
    ["scissors", "paper", "loss"],
];

const outcomeTovalue = {
    "win": 6,
    "draw": 3,
    "loss": 0,
}

const getOutcome = (left: names, right: names) => {
    return pairsAndOutcome.find(line => line[0] === left && line[1] === right)![2];
}

const input = await fs.readFile("src/day-2/input.txt", "utf8");

const rounds = input.split(/\r?\n/).map(round => round.split(" ").map(sign => {
    return signToName[sign];
}));

const result = rounds.reduce((acc, curr) => {
    const a = curr[0];
    const b = curr[1];
    return acc + namesToValue[curr[1]] + outcomeTovalue[getOutcome(a, b)];
}, 0);

// Expected: 12586
console.log(result);

// Run using:
// npx node --loader ts-node/esm src/day-1/pt1.ts